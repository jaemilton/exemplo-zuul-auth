package com.fotorest.perfil;

import java.security.Principal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PerfilController {
	@Autowired
	PerfilRespository perfilRepository;
	
	@Autowired
	UsuarioClient usuarioClient;
	
	@GetMapping("/quem/xablau")
	public Perfil exibirXablau() {
		Perfil perfil = new Perfil();
		
		perfil.setNome("Xablau");
		perfil.setEmail("xablau@gmail.com");
		perfil.setSenha("xablau123");
		
		return perfil;
	}
	
	@GetMapping("/listar")
	public Iterable<Perfil> consultarTodos() {
		return perfilRepository.findAll();
	}
	
	//@GetMapping("/{nome}")
	//public ResponseEntity consultar(@PathVariable String nome) {
	//Principal é utilizado apenas para endpoints que estão autenticados via token
	@GetMapping
	public ResponseEntity consultar(Principal principal) {
		Optional<Perfil> perfilOptional = perfilRepository.findById(principal.getName());
		
		if(perfilOptional.isPresent()) {
			return ResponseEntity.ok(perfilOptional.get());
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping
	public Perfil criar(@RequestBody Perfil perfil) {
		usuarioClient.inserir(perfil);
		return perfilRepository.save(perfil);
	}
}
